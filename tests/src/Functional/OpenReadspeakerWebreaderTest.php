<?php

declare(strict_types=1);

namespace Drupal\Tests\open_readspeaker\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests open_readspeaker_webreader config and settings.
 */
class OpenReadspeakerWebreaderTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['open_readspeaker', 'block'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests ReadSpeaker tokens and block on a page.
   */
  public function testReadSpeakerBlock(): void {
    $token_service = \Drupal::token();
    $account_id = $this->randomMachineName(7);
    $cdn = 'au';
    $language = 'en_au';

    \Drupal::configFactory()
      ->getEditable('open_readspeaker.settings')
      ->set('customerid', $account_id)
      ->set('cdn_region', $cdn)
      ->set('lang', $language)
      ->save(TRUE);

    // Generate and test tokens.
    $tests = [];
    $tests['[open-readspeaker:customer-id]'] = $account_id;
    $tests['[open-readspeaker:cdn]'] = $cdn;

    foreach ($tests as $input => $expected) {
      $output = $token_service->replace($input);
      $this->assertEquals($expected, $output);
    }

    $reading_area = $this->randomMachineName();
    $button_title = $this->randomMachineName();
    $button_text = $this->randomMachineName();
    $this->drupalPlaceBlock('open_readspeaker_webreader', [
      'button_text' => $button_text,
      'button_title' => $button_title,
      'reading_area' => $reading_area,
      'reading_area_class' => '',
    ]);

    $this->drupalGet('<front>');
    $assert = $this->assertSession();
    // ReadSpeaker scripts.
    $assert->responseContains(sprintf('//app-%s.readspeaker.com/cgi-bin/rsent?customerid=%s', $cdn, $account_id));
    $assert->responseContains(sprintf('//cdn-%s.readspeaker.com/script/%s/webReader/webReader.js?pids=wr', $cdn, $account_id));
    // ReadSpeaker button.
    $selector = '.open-readspeaker-webreader .rsbtn_play';
    $assert->elementExists('css', $selector);
    $assert->elementAttributeContains('css', $selector, 'title', $button_title);
    $assert->elementAttributeContains('css', $selector, 'href', 'lang=en_au');
    $assert->elementAttributeContains('css', $selector, 'href', 'readid=' . $reading_area);
    // Empty params shouldn't appear in button href.
    $assert->elementAttributeNotContains('css', $selector, 'href', 'readclass=');
  }

}
