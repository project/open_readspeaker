## Description
This module integrates ReadSpeaker to your Drupal website.

## Table of contents

[[_TOC_]]

## Prerequisites for live site
An own account at ReadSpeaker that includes the ReadSpeaker Enterprise Highlighting product license.

## Local development

For local development, ReadSpeaker kindly offers an account for the products ReadSpeaker webReader and ReadSpeaker docReader, with a limit of 1000 characters.

The developer Account ID is `5324`, which should be used both in the href of the listen button and in the script link.

**Note**: The webReader script of customerid `5324` is set up to always use POST.

These languages are available:

- Danish
- English (UK)
- English (US)
- French
- German
- Italian
- Russian
- Spanish
- Swedish

The access control is limited to these Lando and DDEV subdomains:

- `*.ddev.site`
- `*.lndo.site`

## Configuration
To enable this module visit the module page and enable Open ReadSpeaker module.
You can configure at /admin/config/services/open-readspeaker.

The default URL to the Readspeaker script may differ based on your licensed version or location.
To check your individual URL check the Readspeaker admin panel at:
_Implementation > Implementation details > Page 3 of 7_

## Block configuration

### Reading area ID
That configuration is the only mandatory setting you have to apply.
Set it to the ID (_HTML-Attribute_ provided by theme or theme_hook) of your main reading area.

### Reading area of classes
To add additional reading areas which extend the main reading area on different pages, add additional css classes.

## Applying JavaScript callbacks

To add your custom javascript callbacks to the ReadSpeaker webreader events, simply create a custom javascript file
and define a `library-override` or `library-extend` in your custom module/theme `*.info.yml`.

Example for a `library-override`.

```javascript
// js/open_readspeaker_override.js
(function (drupalSettings) {
  'use strict';
  // Defining the callbacks.
  const callbacks = {
    cb: {
      ui: {
        open: function() {
          console.log('Open Callback!')
        }
      }
    }
  }
  // Merge the 'rsConf' object with our 'callbacks' object.
  window.rsConf = { ...drupalSettings.open_readspeaker.rsConf, ...callbacks };
})(drupalSettings);
```
```yaml
libraries-override:
  # Replace an asset with another.
  open_readspeaker/conf:
    js:
      js/conf.js: js/open_readspeaker_override.js
```

## Developer documentation
See [wrdev.readspeaker.com](https://wrdev.readspeaker.com/)

## Maintainers

- Sven Schüring - [sunlix](https://www.drupal.org/u/sunlix)
- Luigi Guevara - [killua99](https://www.drupal.org/u/killua99)
- Mustakimul Islam - [takim](https://www.drupal.org/u/takim)
