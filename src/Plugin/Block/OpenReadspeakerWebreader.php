<?php

namespace Drupal\open_readspeaker\Plugin\Block;

use Drupal\Core\Block\Attribute\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Url;
use Drupal\Core\Utility\Token;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a 'OpenReadspeakerWebreader' block.
 */
#[Block(
  id: 'open_readspeaker_webreader',
  admin_label: new TranslatableMarkup('Open ReadSpeaker: Webreader')
)]
class OpenReadspeakerWebreader extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The configuration factory.
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The token utility.
   */
  protected Token $token;

  /**
   * The request stack.
   */
  protected RequestStack $requestStack;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    $instance = new static($configuration, $plugin_id, $plugin_definition);

    $instance->configFactory = $container->get('config.factory');
    $instance->token = $container->get('token');
    $instance->requestStack = $container->get('request_stack');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'button_url' => '//app-[open-readspeaker:cdn].readspeaker.com/cgi-bin/rsent',
      'button_text' => 'Listen',
      'button_title' => 'Listen to this page using ReadSpeaker',
      'reading_area' => 'block-olivero-content',
      'reading_area_class' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {

    $form['button_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button URL'),
      '#description' => $this->t('The URL where the Readspeaker is triggered from.'),
      '#default_value' => $this->configuration['button_url'],
      '#required' => TRUE,
    ];

    $form['button_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button text'),
      '#description' => $this->t('Please write button text here.'),
      '#default_value' => $this->configuration['button_text'],
      '#required' => TRUE,
    ];

    $form['button_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button title'),
      '#description' => $this->t('Specify the title attribute for the ReadSpeaker button.'),
      '#default_value' => $this->configuration['button_title'],
    ];

    $form['reading_area'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Reading area ID'),
      '#description' => $this->t('Specify content using HTML ID attribute.'),
      '#default_value' => $this->configuration['reading_area'],
      '#required' => TRUE,
    ];

    $form['reading_area_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Reading area of classes'),
      '#description' => $this->t('Specify content using HTML Class attribute(s). For multiple classes use format: class1,class2,class3'),
      '#default_value' => $this->configuration['reading_area_class'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['button_url'] = $form_state->getValue('button_url');
    $this->configuration['button_text'] = $form_state->getValue('button_text');
    $this->configuration['button_title'] = $form_state->getValue('button_title');
    $this->configuration['reading_area'] = $form_state->getValue('reading_area');
    $this->configuration['reading_area_class'] = $form_state->getValue('reading_area_class');
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $config = $this->configFactory->get('open_readspeaker.settings');
    $customerId = $config->get('customerid');
    $currentRequest = $this->requestStack->getCurrentRequest();
    $currentUrl = Url::fromRoute('<current>', $currentRequest->query->all(), ['absolute' => TRUE])->toString();
    $attributes = new Attribute();
    $attributes->setAttribute('title', $this->configuration['button_title']);
    $buttonUrl = $this->token->replacePlain($this->configuration['button_url']);
    $buttonUrlParameters = [
      'customerid' => $customerId,
      'lang' => $config->get('lang'),
      'voice' => $config->get('voice'),
      'readid' => $this->configuration['reading_area'],
      'readclass' => $this->configuration['reading_area_class'],
      'url' => $currentUrl,
    ];
    $buttonUrlParameters = array_filter($buttonUrlParameters);
    $url = Url::fromUri($buttonUrl, ['query' => $buttonUrlParameters]);

    if (empty($customerId)) {
      $this->messenger()->addWarning($this->t('Please go to @link and fill the customer ID.', [
        '@link' => Link::createFromRoute($this->t('manage Open ReadSpeaker'), 'open_readspeaker.settings')->toString(),
      ]));
      return [];
    }

    $library[] = 'open_readspeaker/basic';
    $library[] = 'open_readspeaker/conf';

    return [
      '#theme' => 'open_readspeaker_webreader',
      '#button_attributes' => $attributes,
      '#button_text' => $this->configuration['button_text'],
      '#url' => $url,
      '#attached' => [
        'library' => $library,
        'drupalSettings' => [
          'open_readspeaker' => ['rsConf' => $config->get('rsConf')],
        ],
      ],
      '#cache' => [
        'contexts' => [
          'url',
        ],
        'tags' => [
          'config:open_readspeaker.settings',
        ],
      ],
    ];
  }

}
