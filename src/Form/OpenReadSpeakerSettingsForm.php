<?php

namespace Drupal\open_readspeaker\Form;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\ConfigTarget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * The setting form for the open_readspeaker settings.
 *
 * @package Drupal\open_readspeaker\Form
 */
class OpenReadSpeakerSettingsForm extends ConfigFormBase {

  const CONFIG_OBJECT_NAME = 'open_readspeaker.settings';

  /**
   * The module handler.
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The renderer.
   */
  protected RendererInterface $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);

    $instance->moduleHandler = $container->get('module_handler');
    $instance->renderer = $container->get('renderer');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      self::CONFIG_OBJECT_NAME,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'open_read_speaker_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config(self::CONFIG_OBJECT_NAME);

    $form['global'] = [
      '#title' => $this->t('General settings for ReadSpeaker'),
      '#type' => 'fieldset',
    ];

    $form['global']['customerId'] = [
      '#title' => $this->t('Customer ID'),
      '#description' => $this->t('Enter your ReadSpeaker customer ID'),
      '#type' => 'textfield',
      '#config_target' => self::CONFIG_OBJECT_NAME . ':customerid',
      '#required' => TRUE,
    ];

    $form['global']['cdnRegion'] = [
      '#title' => $this->t('CDN'),
      '#description' => $this->t('Select the CDN for your region'),
      '#type' => 'select',
      '#options' => self::validCdnChoices(TRUE),
      '#config_target' => self::CONFIG_OBJECT_NAME . ':cdn_region',
      '#required' => TRUE,
    ];

    $form['global']['lang'] = [
      '#title' => $this->t('Language'),
      '#description' => $this->t('Select which language your ReadSpeaker account supports.'),
      '#type' => 'select',
      '#options' => self::validLanguageChoices(TRUE),
      '#config_target' => self::CONFIG_OBJECT_NAME . ':lang',
      '#required' => TRUE,
    ];

    $form['global']['voice'] = [
      '#title' => $this->t('Voice'),
      '#description' => $this->t('Enter male or female language voice name. English language examples: Alice or Hugh. Only relevant in case your account has more than one voice per language. Find available voices under Customer Portal > "Voices"'),
      '#type' => 'textfield',
      '#config_target' => self::CONFIG_OBJECT_NAME . ':voice',
    ];

    if ($this->moduleHandler->moduleExists('token')) {
      $form['global']['token_browser'] = [
        '#theme' => 'token_tree_link',
        '#token_types' => [
          'open_readspeaker',
        ],
      ];
      $tokenBrowserLink = $this->renderer->render($form['global']['token_browser']);
    }

    $form['global']['webreader_url'] = [
      '#title' => $this->t('URL'),
      '#description' => $this->t('The remote URL of the ReadSpeaker JavaScript.') . ($tokenBrowserLink ?? ''),
      '#type' => 'textfield',
      '#config_target' => self::CONFIG_OBJECT_NAME . ':webreader_url',
      '#required' => TRUE,
    ];

    $form['tabs'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('ReadSpeaker widget configuration'),
      '#attached' => [
        'library' => [
          'open_readspeaker/open_readspeaker.admin',
        ],
      ],
    ];

    $rsConf = $config->get('rsConf');

    // General specific configurations.
    $form['general'] = [
      '#type' => 'details',
      '#title' => $this->t('General'),
      '#group' => 'tabs',
      '#weight' => -1,
    ];

    $form['general']['confirmPolicy'] = [
      '#title' => $this->t('Confirm policy'),
      '#description' => $this->t('When set, the user will need to read and confirm a message before the playback can start on first click.'),
      '#type' => 'textfield',
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.general.confirmPolicy',
    ];

    $form['general']['cookieLifetime'] = [
      '#title' => $this->t('Cookie lifetime'),
      '#description' => $this->t('How long the settings cookie should survive on the user’s computer, expressed in milliseconds.'),
      '#type' => 'number',
      '#min' => -1,
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.general.cookieLifetime',
    ];

    $form['general']['cookieName'] = [
      '#title' => $this->t('Cookie name'),
      '#description' => $this->t('The name of the main cookie that will be used to store users’ personalized settings.'),
      '#type' => 'textfield',
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.general.cookieName',
    ];

    $form['general']['customTransLangs'] = [
      '#title' => $this->t('Translation languages'),
      '#description' => $this->t('Overrides the default target languages list.'),
      '#type' => 'checkboxes',
      '#options' => self::validTranslationChoices(TRUE),
      '#config_target' => new ConfigTarget(
        self::CONFIG_OBJECT_NAME,
        'rsConf.general.customTransLangs',
        NULL,
        fn($value) => array_keys(array_filter($value)),
      ),
    ];

    $form['general']['defaultSpeedValue'] = [
      '#title' => $this->t('Default speed value'),
      '#description' => $this->t('This is the default speed value sent to ReadSpeaker.'),
      '#type' => 'number',
      '#min' => 0,
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.general.defaultSpeedValue',
    ];

    $form['general']['domain'] = [
      '#title' => $this->t('Domain'),
      '#description' => $this->t('The domain that is used for the calls to the ReadSpeaker servers.'),
      '#type' => 'textfield',
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.general.domain',
    ];

    $form['general']['nativeLanguages'] = [
      '#title' => $this->t('Native languages'),
      '#description' => $this->t('The languages in Reading Voice and Translation can be displayed as their own localized names.'),
      '#type' => 'checkbox',
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.general.nativeLanguages',
    ];

    $form['general']['popupCloseTime'] = [
      '#title' => $this->t('Popup close time'),
      '#description' => $this->t('Determines the amount of time the popup will be visible when the user has selected some text. Expressed in milliseconds.'),
      '#type' => 'number',
      '#min' => 0,
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.general.popupCloseTime',
    ];

    $form['general']['shadowDomSelector'] = [
      '#title' => $this->t('Shadow DOM selector'),
      '#description' => $this->t('Enable reading content from a shadow DOM.'),
      '#type' => 'textfield',
      '#config_target' => new ConfigTarget(
        self::CONFIG_OBJECT_NAME,
        'rsConf.general.shadowDomSelector',
        NULL,
        fn($value) => empty($value) ? NULL : $value
      ),
    ];

    $form['general']['subdomain'] = [
      '#title' => $this->t('Subdomain'),
      '#description' => $this->t('The sub-domain that is used for the calls to the ReadSpeaker servers.'),
      '#type' => 'textfield',
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.general.subdomain',
    ];

    $form['general']['syncContainer'] = [
      '#title' => $this->t('Sync container'),
      '#description' => $this->t('Sets the type of element the sync tags will use.'),
      '#type' => 'textfield',
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.general.syncContainer',
    ];

    $form['general']['saveLangVoice'] = [
      '#title' => $this->t('Save language voice'),
      '#description' => $this->t('The language selected in the voice settings tool will be stored in a cookie.'),
      '#type' => 'checkbox',
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.general.saveLangVoice',
    ];

    $form['general']['translatedDisclaimer'] = [
      '#title' => $this->t('Translated disclaimer'),
      '#description' => $this->t('The translation disclaimer can be displayed in the target language.'),
      '#type' => 'checkbox',
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.general.translatedDisclaimer',
    ];

    $form['general']['skipHiddenContent'] = [
      '#title' => $this->t('Skip hidden content'),
      '#description' => $this->t('By using POST mode and Skip hidden content option, any non-visible content will be ignored at reading. This works for elements with CSS style <code>display:none</code>, with CSS style <code>visibility:hidden</code> or with HTML hidden attribute.'),
      '#type' => 'checkbox',
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.general.skipHiddenContent',
    ];

    $form['general']['labelsIgnoreSelector'] = [
      '#title' => $this->t('Ignore selector'),
      '#description' => $this->t('Skip html attribute values from being read, e.g <em>aria-label</em>. You can add multiple comma separated values.'),
      '#type' => 'textfield',
      '#config_target' => new ConfigTarget(
        self::CONFIG_OBJECT_NAME,
        'rsConf.general.labels.ignoreSelector',
        fn($value) => empty($value) ? '' : implode(', ', array_map(fn($value) => mb_substr(trim($value), 1, mb_strlen(trim($value)) - 2), explode(',', $value))),
        fn($value) => empty($value) ? '' : implode(', ', array_map(fn($value) => '[' . trim($value) . ']', explode(',', $value))),
      ),
    ];

    $form['general']['usePost'] = [
      '#title' => $this->t('Use POST mode'),
      '#description' => $this->t('Enabling this option will POST the html/text to the ReadSpeaker service instead of ReadSpeaker accessing your site, which will make the service work behind password protected pages.'),
      '#type' => 'checkbox',
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.general.usePost',
      '#states' => [
        'required' => [':input[name="skipHiddenContent"]' => ['checked' => TRUE]],
      ],
    ];

    // Settings specific configurations.
    $form['settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Settings'),
      '#group' => 'tabs',
    ];

    $form['settings']['hl'] = [
      '#title' => $this->t('Highlight style'),
      '#description' => $this->t('Which highlighting style to use.'),
      '#type' => 'select',
      '#options' => [
        'wordsent' => $this->t('Word & sentences'),
        'word' => $this->t('Word'),
        'sent' => $this->t('Sentences'),
      ],
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.settings.hl',
    ];

    $form['settings']['hlicon'] = [
      '#title' => $this->t('Highlight icon'),
      '#description' => $this->t('Whether or not to display a popup button when selecting text on a web page.'),
      '#type' => 'select',
      '#options' => [
        'iconon' => $this->t('On'),
        'iconoff' => $this->t('Off'),
      ],
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.settings.hlicon',
    ];

    $form['settings']['hlscroll'] = [
      '#title' => $this->t('Highlight scroll'),
      '#description' => $this->t('Whether or not to automatically scroll the page vertically when the reading has reached the bottom of the browser’s viewport.'),
      '#type' => 'select',
      '#options' => [
        'scrollon' => $this->t('On'),
        'scrolloff' => $this->t('Off'),
      ],
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.settings.hlscroll',
    ];

    $form['settings']['hlsent'] = [
      '#title' => $this->t('Highlighting sentences'),
      '#description' => $this->t('The color that will be used for highlighting sentences, expressed as a valid CSS color value.'),
      '#type' => 'color',
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.settings.hlsent',
    ];

    $form['settings']['hlspeed'] = [
      '#title' => $this->t('Highlighting speed'),
      '#description' => $this->t('The reading speed.'),
      '#type' => 'select',
      '#options' => [
        'slow' => $this->t('Slow'),
        'medium' => $this->t('Medium'),
        'fast' => $this->t('Fast'),
      ],
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.settings.hlspeed',
    ];

    $form['settings']['hltext'] = [
      '#title' => $this->t('Highlighting text'),
      '#description' => $this->t('The color that will be used for the text in the highlighted elements, expressed as a valid CSS color value.'),
      '#type' => 'color',
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.settings.hltext',
    ];

    $form['settings']['hltoggle'] = [
      '#title' => $this->t('Highlighting toggle'),
      '#description' => $this->t('This setting toggles highlighting on or off (hlon or hloff).'),
      '#type' => 'select',
      '#options' => [
        'hlon' => $this->t('On'),
        'hloff' => $this->t('Off'),
      ],
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.settings.hltoggle',
    ];

    $form['settings']['hlword'] = [
      '#title' => $this->t('Highlighting words'),
      '#description' => $this->t('The color that will be used for highlighting words, expressed as a valid CSS color value.'),
      '#type' => 'color',
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.settings.hlword',
    ];

    // Keyboard shortcuts specific configurations.
    $form['keyboard'] = [
      '#type' => 'details',
      '#title' => $this->t('Keyboard combinations'),
      '#group' => 'tabs',
    ];

    $form['keyboard']['kbClicklisten'] = [
      '#title' => $this->t('Click listen'),
      '#description' => $this->t('Keyboard combination for "clicklisten".'),
      '#type' => 'textfield',
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.settings.kb.clicklisten',
    ];

    $form['keyboard']['kbControlpanel'] = [
      '#title' => $this->t('Control panel'),
      '#description' => $this->t('Keyboard combination for "controlpanel" action.'),
      '#type' => 'textfield',
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.settings.kb.controlpanel',
    ];

    $form['keyboard']['kbDictionary'] = [
      '#title' => $this->t('Dictionary'),
      '#description' => $this->t('Keyboard combination for "dictionary" action.'),
      '#type' => 'textfield',
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.settings.kb.dictionary',
    ];

    $form['keyboard']['kbDownload'] = [
      '#title' => $this->t('Download'),
      '#description' => $this->t('Keyboard combination for "download" action.'),
      '#type' => 'textfield',
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.settings.kb.download',
    ];

    $form['keyboard']['kbEnlarge'] = [
      '#title' => $this->t('Enlarge'),
      '#description' => $this->t('Keyboard combination for "enlarge" action.'),
      '#type' => 'textfield',
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.settings.kb.enlarge',
    ];

    $form['keyboard']['kbFontsizeminus'] = [
      '#title' => $this->t('Decrease font size'),
      '#description' => $this->t('Keyboard combination for "fontsizeminus" action.'),
      '#type' => 'textfield',
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.settings.kb.fontsizeminus',
    ];

    $form['keyboard']['kbFontsizeplus'] = [
      '#title' => $this->t('Increase font size'),
      '#description' => $this->t('Keyboard combination for "fontsizeplus" action.'),
      '#type' => 'textfield',
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.settings.kb.fontsizeplus',
    ];

    $form['keyboard']['kbFormreading'] = [
      '#title' => $this->t('Form reading'),
      '#description' => $this->t('Keyboard combination for "formreading" action.'),
      '#type' => 'textfield',
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.settings.kb.formreading',
    ];

    $form['keyboard']['kbHelp'] = [
      '#title' => $this->t('Help'),
      '#description' => $this->t('Keyboard combination for "help" action.'),
      '#type' => 'textfield',
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.settings.kb.help',
    ];

    $form['keyboard']['kbMenu'] = [
      '#title' => $this->t('Menu'),
      '#description' => $this->t('Keyboard combination for "menu" action.'),
      '#type' => 'textfield',
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.settings.kb.menu',
    ];

    $form['keyboard']['kbPagemask'] = [
      '#title' => $this->t('Page mask'),
      '#description' => $this->t('Keyboard combination for "pagemask" action.'),
      '#type' => 'textfield',
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.settings.kb.pagemask',
    ];

    $form['keyboard']['kbPause'] = [
      '#title' => $this->t('Pause'),
      '#description' => $this->t('Keyboard combination for "pause" action.'),
      '#type' => 'textfield',
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.settings.kb.pause',
    ];

    $form['keyboard']['kbPlay'] = [
      '#title' => $this->t('Play'),
      '#description' => $this->t('Keyboard combination for "play" action.'),
      '#type' => 'textfield',
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.settings.kb.play',
    ];

    $form['keyboard']['kbPlayerfocus'] = [
      '#title' => $this->t('Player focus'),
      '#description' => $this->t('Keyboard combination for "playerfocus" action.'),
      '#type' => 'textfield',
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.settings.kb.playerfocus',
    ];

    $form['keyboard']['kbSettings'] = [
      '#title' => $this->t('Settings'),
      '#description' => $this->t('Keyboard combination for "settings" action.'),
      '#type' => 'textfield',
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.settings.kb.settings',
    ];

    $form['keyboard']['kbStop'] = [
      '#title' => $this->t('Stop'),
      '#description' => $this->t('Keyboard combination for "stop" action.'),
      '#type' => 'textfield',
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.settings.kb.stop',
    ];

    $form['keyboard']['kbTextmode'] = [
      '#title' => $this->t('Text mode'),
      '#description' => $this->t('Keyboard combination for "textmode" action.'),
      '#type' => 'textfield',
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.settings.kb.textmode',
    ];

    $form['keyboard']['kbTranslation'] = [
      '#title' => $this->t('Translation'),
      '#description' => $this->t('Keyboard combination for "translation" action.'),
      '#type' => 'textfield',
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.settings.kb.translation',
    ];

    $form['keyboard']['kbReadingvoice'] = [
      '#title' => $this->t('Reading voice'),
      '#description' => $this->t('Keyboard combination for "readingvoice" action.'),
      '#type' => 'textfield',
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.settings.kb.readingvoice',
    ];

    $form['keyboard']['kbDetachfocus'] = [
      '#title' => $this->t('Detach focus'),
      '#description' => $this->t('Keyboard combination for "detachfocus" action.'),
      '#type' => 'textfield',
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.settings.kb.detachfocus',
    ];

    // UI specific configurations.
    $form['ui'] = [
      '#type' => 'details',
      '#title' => $this->t('User interface'),
      '#group' => 'tabs',
    ];

    $uiConfig = $rsConf['ui'];

    $form['ui']['mobileVertPos'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Mobile vertical position'),
    ];

    [$mobileVertAlign, $mobileVertOffset] = explode('=', $uiConfig['mobileVertPos']);

    $form['ui']['mobileVertPos']['vertAlign'] = [
      '#title' => $this->t('Alignment'),
      '#description' => $this->t('The vertical alignment of the mobile menu.'),
      '#type' => 'radios',
      '#options' => [
        'top' => $this->t('Top'),
        'bottom' => $this->t('Bottom'),
      ],
      '#default_value' => $mobileVertAlign,
      '#required' => TRUE,
    ];

    $form['ui']['mobileVertPos']['vertOffset'] = [
      '#title' => $this->t('Offset'),
      '#description' => $this->t('The vertical offset of the mobile menu.'),
      '#type' => 'number',
      '#min' => 0,
      '#default_value' => $mobileVertOffset,
      '#required' => TRUE,
    ];

    $form['ui']['controlpanelVertical'] = [
      '#title' => $this->t('Default vertical position'),
      '#description' => $this->t('Change the default vertical alignment.'),
      '#type' => 'select',
      '#options' => [
        'top' => $this->t('Top'),
        'bottom' => $this->t('Bottom'),
      ],
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.ui.controlpanel.vertical',
    ];

    $form['ui']['controlpanelHorizontal'] = [
      '#title' => $this->t('Default horizontal position'),
      '#description' => $this->t('Change the default the horizontal alignment.'),
      '#type' => 'select',
      '#options' => [
        'left' => $this->t('Left'),
        'right' => $this->t('Right'),
      ],
      '#config_target' => self::CONFIG_OBJECT_NAME . ':rsConf.ui.controlpanel.horizontal',
    ];

    $form['ui']['tools'] = [
      '#title' => $this->t('Tools'),
      '#description' => $this->t('Enable / Disable specific tool menu entries.'),
      '#type' => 'checkboxes',
      '#options' => [
        'settings' => $this->t('Settings'),
        'voicesettings' => $this->t('Voice settings'),
        'clicklisten' => $this->t('Click listen'),
        'enlarge' => $this->t('Enlarge'),
        'formreading' => $this->t('Form reading'),
        'textmode' => $this->t('Text mode'),
        'pagemask' => $this->t('Page mask'),
        'download' => $this->t('Download'),
        'help' => $this->t('Help'),
        'dictionary' => $this->t('Dictionary'),
        'translation' => $this->t('Translation'),
        'skipbuttons' => $this->t('Skip buttons'),
        'speedbutton' => $this->t('Speed button'),
        'controlpanel' => $this->t('Control panel'),
      ],
      '#config_target' => new ConfigTarget(
        self::CONFIG_OBJECT_NAME,
        'rsConf.ui.tools',
        fn($value) => array_keys(array_filter($value)),
        fn($value) => array_map(fn($value) => (bool) $value, $value)
      ),
    ];

    // @todo implement 'phrases' setting.
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);

    $mobileVertPos = $form_state->getValue('vertAlign') . '=' . $form_state->getValue('vertOffset');

    $this->config('open_readspeaker.settings')
      ->set('rsConf.ui.mobileVertPos', $mobileVertPos)
      ->save();
  }

  /**
   * Validation callback for the 'ignoreSelector' config property.
   *
   * @param string $value
   *   The value to validate.
   * @param \Symfony\Component\Validator\Context\ExecutionContextInterface $context
   *   The validation execution context.
   *
   * @return void
   *   Add violations to the validation context.
   */
  public static function validateIgnoreSelector(string $value, ExecutionContextInterface $context): void {
    // Ignore NULL values (i.e. support optional value).
    if (empty($value)) {
      return;
    }

    $ignoreAttributes = array_map(fn($value) => mb_substr(trim($value), 1, mb_strlen(trim($value)) - 2), explode(',', $value));
    $validAttributes = array_map(fn($attribute) => preg_match('/^[\w-]+$/', $attribute), $ignoreAttributes);
    $errorAttributes = array_diff_key($ignoreAttributes, array_filter($validAttributes));

    if ($errorAttributes) {
      $context->addViolation('The attributes %attributes must only match word characters and dashes.', [
        '%attributes' => implode(', ', $errorAttributes),
      ]);
    }
  }

  /**
   * Get the options with labels for form API or without config schema callback.
   *
   * @param bool $withLabels
   *   Option to get option with labels.
   *
   * @return array
   *   The valid choices.
   */
  public static function validLanguageChoices(bool $withLabels = FALSE): array {
    $translationManager = \Drupal::translation();
    $options = [
      'none'  => $translationManager->translate('Select a language'),
      'ar_ar' => $translationManager->translate('@arabic - Arabic', ['@arabic' => urldecode('%D8%B9%D8%B1%D8%A8%D9%8A')]),
      'ca_es' => $translationManager->translate('Català - Catalan'),
      'cs_cz' => $translationManager->translate('Čeština - Czech'),
      'cy_cy' => $translationManager->translate('Cymraeg - Welsh'),
      'da_dk' => $translationManager->translate('Dansk - Danish'),
      'de_de' => $translationManager->translate('Deutsch - German'),
      'en_in' => $translationManager->translate('English (Indian) - English'),
      'en_us' => $translationManager->translate('English (US) - English'),
      'en_uk' => $translationManager->translate('English (British) - English'),
      'en_au' => $translationManager->translate('English (Australia) - English'),
      'en_sc' => $translationManager->translate('English (Scottish) - English'),
      'en_za' => $translationManager->translate('English (South African) - English'),
      'es_ar' => $translationManager->translate('Español (de AR) - Spanish'),
      'es_co' => $translationManager->translate('Español (de Colombia) - Spanish'),
      'es_es' => $translationManager->translate('Español (de España) - Spanish'),
      'es_mx' => $translationManager->translate('Español (de Mexico) - Spanish'),
      'es_us' => $translationManager->translate('Español (de US) - Spanish'),
      'eu_es' => $translationManager->translate('Euskara - Basque'),
      'el_gr' => $translationManager->translate('Ελληνικά - Greek'),
      'fa_ir' => $translationManager->translate('فارسی - Farsi'),
      'fo_fo' => $translationManager->translate('Føroyskt - Faroese'),
      'fi_fi' => $translationManager->translate('Suomi - Finnish'),
      'fr_fr' => $translationManager->translate('Français - French'),
      'fr_be' => $translationManager->translate('Français (Belge) - French (Belgian)'),
      'fr_ca' => $translationManager->translate('Français (Canadian) - French (Canadian)'),
      'fy_nl' => $translationManager->translate('Frysk - Frisian'),
      'gl_es' => $translationManager->translate('Galego - Galician'),
      'he_il' => $translationManager->translate('עִברִית - Hebrew'),
      'hi_in' => $translationManager->translate('@hindi - Hindi', ['@hindi' => urldecode('%E0%A4%B9%E0%A4%BF%E0%A4%A8%E0%A5%8D%E0%A4%A6%E0%A5%80%20(%E0%A4%AD%E0%A4%BE%E0%A4%B0%E0%A4%A4)')]),
      'hr_hr' => $translationManager->translate('Hrvatski - Croatian'),
      'id_id' => $translationManager->translate('Bahasa Indonesia - Indonesian'),
      'it_it' => $translationManager->translate('Italiano - Italian'),
      'is_is' => $translationManager->translate('íslenska - Icelandic'),
      'ja_jp' => $translationManager->translate('日本語 - Japanese'),
      'ko_kr' => $translationManager->translate('한국어 - Korean'),
      'lv_lv' => $translationManager->translate('Latviski - Latvian'),
      'pl_pl' => $translationManager->translate('Polski - Polish'),
      'pt_br' => $translationManager->translate('Português (Brasil) - Portuguese'),
      'pt_pt' => $translationManager->translate('Português (Europeu) - Portuguese'),
      'ro_ro' => $translationManager->translate('Română - Romanian'),
      'nl_nl' => $translationManager->translate('Nederlands - Dutch'),
      'nl_be' => $translationManager->translate('Nederlands (Belgisch) - Dutch (Belgian)'),
      'no_nb' => $translationManager->translate('Bokmål - Norwegian'),
      'no_nn' => $translationManager->translate('Nynorsk - Norwegian'),
      'ru_ru' => $translationManager->translate('Русский - Russian'),
      'sk_sk' => $translationManager->translate('Slovenčina - Slovak'),
      'sv_se' => $translationManager->translate('Svenska - Swedish'),
      'th_th' => $translationManager->translate('@thai - Thai', ['@thai' => urldecode('%E0%B9%84%E0%B8%97%E0%B8%A2')]),
      'tr_tr' => $translationManager->translate('Türkçe - Turkish'),
      'uk_ua' => $translationManager->translate('Українська - Ukrainian'),
      'zh_cn' => $translationManager->translate('@mandarin - Mandarin Chinese', ['@mandarin' => urldecode('%E4%B8%AD%E6%96%87%20(%E7%AE%80%E4%BD%93)')]),
      'zh_hk' => $translationManager->translate('@traditional - Traditional Chinese', ['@traditional' => urldecode('%E4%B8%AD%E5%9C%8B%EF%BC%88%E7%B9%81%E9%AB%94%EF%BC%89')]),
      'zh_tw' => $translationManager->translate('@taiwanese - Taiwanese Chinese', ['@taiwanese' => urldecode('%E4%B8%AD%E6%96%87+%28%E8%87%BA%E7%81%A3%E5%9C%8B%E8%AA%9E%29')]),
    ];

    return $withLabels ? $options : array_keys($options);
  }

  /**
   * Get the options with labels for form API or without config schema callback.
   *
   * @param bool $withLabels
   *   Option to get option with labels.
   *
   * @return array
   *   The valid choices.
   */
  public static function validTranslationChoices(bool $withLabels = FALSE): array {
    $translationManager = \Drupal::translation();
    $options = [
      'ar_ar' => $translationManager->translate('@arabic - Arabic', ['@arabic' => urldecode('%D8%B9%D8%B1%D8%A8%D9%8A')]),
      'da_dk' => $translationManager->translate('Dansk - Danish'),
      'de_de' => $translationManager->translate('Deutsch - German'),
      'el_gr' => $translationManager->translate('Ελληνικά - Greek'),
      'en' => $translationManager->translate('English - English'),
      'es_es' => $translationManager->translate('Español - Spanish'),
      'fi_fi' => $translationManager->translate('Suomi - Finnish'),
      'fr_fr' => $translationManager->translate('Français - French'),
      'hu_hu' => $translationManager->translate('Magyar - Hungarian'),
      'id_id' => $translationManager->translate('Bahasa Indonesia - Indonesian'),
      'it_it' => $translationManager->translate('Italiano - Italian'),
      'ja_jp' => $translationManager->translate('日本語 - Japanese'),
      'ko_kr' => $translationManager->translate('한국어 - Korean'),
      'lv_lv' => $translationManager->translate('Latviski - Latvian'),
      'nl_nl' => $translationManager->translate('Nederlands - Dutch'),
      'no_nb' => $translationManager->translate('Bokmål - Norwegian'),
      'pl_pl' => $translationManager->translate('Polski - Polish'),
      'pt_br' => $translationManager->translate('Português (Brasil) - Portuguese'),
      'pt_pt' => $translationManager->translate('Português (Europeu) - Portuguese'),
      'ro_ro' => $translationManager->translate('Română - Romanian'),
      'ru_ru' => $translationManager->translate('Русский - Russian'),
      'sv_se' => $translationManager->translate('Svenska - Swedish'),
      'th_th' => $translationManager->translate('@thai - Thai', ['@thai' => urldecode('%E0%B9%84%E0%B8%97%E0%B8%A2')]),
      'tr_tr' => $translationManager->translate('Türkçe - Turkish'),
      'uk_ua' => $translationManager->translate('Українська - Ukrainian'),
      'vi_vn' => $translationManager->translate('Tiếng Việt - Vietnamese'),
      'zh_cn' => $translationManager->translate('@mandarin - Mandarin Chinese', ['@mandarin' => urldecode('%E4%B8%AD%E6%96%87%20(%E7%AE%80%E4%BD%93)')]),
    ];

    return $withLabels ? $options : array_keys($options);
  }

  /**
   * Get the options with labels for form API or without config schema callback.
   *
   * @param bool $withLabels
   *   Option to get option with labels.
   *
   * @return array
   *   The valid choices.
   */
  public static function validCdnChoices(bool $withLabels = FALSE): array {
    $translationManager = \Drupal::translation();
    $options = [
      'eu' => $translationManager->translate('Europe'),
      'me' => $translationManager->translate('Middle East'),
      'af' => $translationManager->translate('Africa'),
      'as' => $translationManager->translate('Asia'),
      'oc' => $translationManager->translate('Oceania'),
      'eas' => $translationManager->translate('East Asia'),
      'na' => $translationManager->translate('North America'),
      'sa' => $translationManager->translate('South America'),
    ];

    return $withLabels ? $options : array_keys($options);
  }

}
