<?php

namespace Drupal\open_readspeaker\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Utility\Token;
use Drupal\csp\CspEvents;
use Drupal\csp\Event\PolicyAlterEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class CspPolicyAlterSubscriber.
 */
class CspSubscriber implements EventSubscriberInterface {

  /**
   * The CspSubscriber constructor.
   */
  public function __construct(
    protected ConfigFactoryInterface $configFactory,
    protected Token $token,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    if (!class_exists(CspEvents::class)) {
      return [];
    }

    return [
      CspEvents::POLICY_ALTER => 'onCspPolicyAlter',
    ];
  }

  /**
   * Alter CSP policy for tracking requests.
   */
  public function onCspPolicyAlter(PolicyAlterEvent $alterEvent): void {
    $config = $this->configFactory->get('open_readspeaker.settings');
    $policy = $alterEvent->getPolicy();
    $webreaderDomain = parse_url($this->token->replace($config->get('webreader_url')), PHP_URL_HOST);

    $policy->fallbackAwareAppendIfEnabled('style-src', [$webreaderDomain]);
    $policy->fallbackAwareAppendIfEnabled('style-src-elem', [$webreaderDomain]);
  }

}
