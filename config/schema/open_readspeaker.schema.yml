open_readspeaker.settings:
  type: config_object
  label: 'Open ReadSpeaker settings'
  constraints:
    FullyValidatable: ~
  mapping:
    lang:
      type: label
      label: 'Language'
      constraints:
        Choice:
          callback: 'Drupal\open_readspeaker\Form\OpenReadSpeakerSettingsForm::validLanguageChoices'
    voice:
      type: label
      label: 'Voice'
      nullable: true
    customerid:
      type: string
      label: 'Enter your ReadSpeaker ID'
      constraints:
        NotBlank: []
    webreader_url:
      type: string
      label: 'The remote URL of the ReadSpeaker JavaScript'
      constraints:
        NotBlank: []
    cdn_region:
      type: string
      label: 'CDN of ReadSpeaker link'
      constraints:
        Choice:
          callback: 'Drupal\open_readspeaker\Form\OpenReadSpeakerSettingsForm::validCdnChoices'
    rsConf:
      type: mapping
      label: 'The rsConf config object.'
      mapping:
        general:
          type: mapping
          mapping:
            confirmPolicy:
              type: string
              label: 'When set, the user will need to read and confirm a message before the playback can start on first click.'
              constraints:
                Regex:
                  # Forbid any kind of control character.
                  # @see https://stackoverflow.com/a/66587087
                  pattern: '/([^\PC])/u'
                  match: false
            cookieLifetime:
              type: integer
              label: 'How long the settings cookie should survive on the user’s computer, expressed in milliseconds.'
              constraints:
                PrimitiveType: {  }
            cookieName:
              type: string
              label: 'The name of the main cookie that will be used to store users’ personalized settings.'
              constraints:
                NotBlank: []
            customTransLangs:
              type: sequence
              label: 'Custom target languages.'
              orderby: value
              requiredKey: false
              sequence:
                type: string
                label: 'Language short codes'
                constraints:
                  Choice:
                    callback: 'Drupal\open_readspeaker\Form\OpenReadSpeakerSettingsForm::validTranslationChoices'
            defaultSpeedValue:
              type: integer
              label: 'This is the default speed value sent to ReadSpeaker.'
              constraints:
                PrimitiveType: {  }
            domain:
              type: string
              label: 'The domain that is used for the calls to the ReadSpeaker servers.'
              constraints:
                NotBlank: []
            nativeLanguages:
              type: boolean
              label: 'The languages in Reading Voice and Translation can be displayed as their own localized names.'
            popupCloseTime:
              type: integer
              label: 'Determines the amount of time the popup will be visible when the user has selected some text. Expressed in milliseconds.'
              constraints:
                PrimitiveType: {  }
            shadowDomSelector:
              type: string
              label: 'Enable reading content from a shadow DOM.'
              nullable: true
              constraints:
                PrimitiveType: {  }
            subdomain:
              type: string
              label: 'The sub-domain that is used for the calls to the ReadSpeaker servers.'
              constraints:
                NotBlank: []
            syncContainer:
              type: string
              label: 'Sets the type of element the sync tags will use.'
              constraints:
                NotBlank: []
            saveLangVoice:
              type: boolean
              label: 'The language selected in the voice settings tool will be stored in a cookie.'
            translatedDisclaimer:
              type: boolean
              label: 'The translation disclaimer can be displayed in the target language.'
            skipHiddenContent:
              type: boolean
              label: 'Enables skipping hidden content.'
            labels:
              type: mapping
              label: 'Config map for additional label scoped configs'
              mapping:
                ignoreSelector:
                  type: string
                  label: 'Comma separated list of HTML attributes prevented to read'
                  constraints:
                    Callback: ['Drupal\open_readspeaker\Form\OpenReadSpeakerSettingsForm', 'validateIgnoreSelector']
            usePost:
              type: boolean
              label: 'Use POST mode.'
        settings:
          type: mapping
          label: 'The settings only affect the current user in the current browser.'
          mapping:
            hl:
              type: string
              label: 'Which highlighting style to use.'
              constraints:
                Choice:
                  - wordsent
                  - word
                  - sent
            hlicon:
              type: string
              label: 'Whether or not to display a popup button when selecting text on a web page.'
              constraints:
                Choice:
                  - iconon
                  - iconoff
            hlscroll:
              type: string
              label: 'Whether or not to automatically scroll the page vertically when the reading has reached the bottom of the browser’s viewport.'
              constraints:
                Choice:
                  - scrollon
                  - scrolloff
            hlsent:
              type: color_hex
              label: 'The color that will be used for highlighting sentences, expressed as a valid CSS color value.'
            hlspeed:
              type: string
              label: 'The reading speed.'
              constraints:
                Choice:
                  - slow
                  - medium
                  - fast
            hltext:
              type: color_hex
              label: 'The color that will be used for the text in the highlighted elements, expressed as a valid CSS color value.'
            hltoggle:
              type: string
              label: 'This setting toggles highlighting on or off (hlon or hloff).'
              constraints:
                Choice:
                  - hlon
                  - hloff
            hlword:
              type: color_hex
              label: 'The color that will be used for highlighting words, expressed as a valid CSS color value.'
            kb:
              type: mapping
              label: 'Keyboard combinations (keyboard shortcuts) to activate features.'
              mapping:
                clicklisten:
                  type: string
                  label: 'Keyboard combination for clicklisten'
                  constraints:
                    NotBlank: []
                controlpanel:
                  type: string
                  label: 'Keyboard combination for controlpanel'
                  constraints:
                    NotBlank: []
                dictionary:
                  type: string
                  label: 'Keyboard combination for dictionary'
                  constraints:
                    NotBlank: []
                download:
                  type: string
                  label: 'Keyboard combination for download'
                  constraints:
                    NotBlank: []
                enlarge:
                  type: string
                  label: 'Keyboard combination for enlarge'
                  constraints:
                    NotBlank: []
                fontsizeminus:
                  type: string
                  label: 'Keyboard combination for fontsizeminus'
                  constraints:
                    NotBlank: []
                fontsizeplus:
                  type: string
                  label: 'Keyboard combination for fontsizeplus'
                  constraints:
                    NotBlank: []
                formreading:
                  type: string
                  label: 'Keyboard combination for formreading'
                  constraints:
                    NotBlank: []
                help:
                  type: string
                  label: 'Keyboard combination for help'
                  constraints:
                    NotBlank: []
                menu:
                  type: string
                  label: 'Keyboard combination for menu'
                  constraints:
                    NotBlank: []
                pagemask:
                  type: string
                  label: 'Keyboard combination for pagemask'
                  constraints:
                    NotBlank: []
                pause:
                  type: string
                  label: 'Keyboard combination for pause'
                  constraints:
                    NotBlank: []
                play:
                  type: string
                  label: 'Keyboard combination for play'
                  constraints:
                    NotBlank: []
                playerfocus:
                  type: string
                  label: 'Keyboard combination for playerfocus'
                  constraints:
                    NotBlank: []
                settings:
                  type: string
                  label: 'Keyboard combination for settings'
                  constraints:
                    NotBlank: []
                stop:
                  type: string
                  label: 'Keyboard combination for stop'
                  constraints:
                    NotBlank: []
                textmode:
                  type: string
                  label: 'Keyboard combination for textmode'
                  constraints:
                    NotBlank: []
                translation:
                  type: string
                  label: 'Keyboard combination for translation'
                  constraints:
                    NotBlank: []
                readingvoice:
                  type: string
                  label: 'Keyboard combination for readingvoice'
                  constraints:
                    NotBlank: []
                detachfocus:
                  type: string
                  label: 'Keyboard combination for detachfocus'
                  constraints:
                    NotBlank: []
        ui:
          type: mapping
          mapping:
            mobileVertPos:
              type: string
              label: 'The mobile Menus vertical placement.'
              constraints:
                NotBlank: []
                Regex:
                  pattern: '/^(?:bottom|top)=[0-9]+$/'
            controlpanel:
              type: mapping
              label: 'Change the default alignment.'
              mapping:
                vertical:
                  type: string
                  label: 'Change the default vertical alignment.'
                  constraints:
                    Choice:
                      - top
                      - bottom
                horizontal:
                  type: string
                  label: 'Change the default the horizontal alignment.'
                  constraints:
                    Choice:
                      - left
                      - right
            tools:
              type: mapping
              label: 'Tool configuration for the webReader.'
              mapping:
                settings:
                  type: boolean
                  label: 'Settings tool.'
                voicesettings:
                  type: boolean
                  label: 'Voice settings tool.'
                clicklisten:
                  type: boolean
                  label: 'Click listen tool.'
                enlarge:
                  type: boolean
                  label: 'Enlarge tool.'
                formreading:
                  type: boolean
                  label: 'Form reading tool.'
                textmode:
                  type: boolean
                  label: 'Text mode tool.'
                pagemask:
                  type: boolean
                  label: 'Page mask tool.'
                download:
                  type: boolean
                  label: 'Download tool.'
                help:
                  type: boolean
                  label: 'Help tool.'
                dictionary:
                  type: boolean
                  label: 'Dictionary tool.'
                translation:
                  type: boolean
                  label: 'Translations tool.'
                skipbuttons:
                  type: boolean
                  label: 'Disable the skip-buttons.'
                speedbutton:
                  type: boolean
                  label: 'Disable the speed button.'
                controlpanel:
                  type: boolean
                  label: 'Disable the floating Control Panel.'
#        @todo implement 'phrases' setting.
#        phrases:
#          type: mapping
#          label: 'The phrases section defines the translation of labels that are used in the user interface.'
#          nullable: true

block.settings.open_readspeaker_webreader:
  type: block_settings
  label: 'Open ReadSpeaker'
  mapping:
    button_url:
      type: string
      label: 'The button URL for the ReadSpeaker request'
    button_text:
      type: label
      label: 'Button text'
    button_title:
      type: label
      label: 'Button title'
    reading_area:
      type: string
      label: 'Reading area ID'
    reading_area_class:
      type: string
      label: 'Reading area of classes'
